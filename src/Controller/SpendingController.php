<?php

namespace App\Controller;

use App\Entity\Spending;
use App\Form\SpendingType;
use App\Entity\Participant;
use App\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/spending")
 */
class SpendingController extends AbstractController
{
   
/**
     * @Route("/spend", name="spending", methods="GET|POST")
     */
    public function new(Request $request) : Response
    {
        $participant = new Participant();

        $campaign_id = $request->request->get('campaign_id');

        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));

        $campaign = $this->getDoctrine()->getRepository(Campaign::class)->find($campaign_id);
        
        $participant->setCampaign($campaign);


        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        $spending = new Spending();
       
        $amount = (int)$request->request->get('amount') * 100;
        $spend = $request->request->get('content');

        $spending->setAmount($amount);
        $spending->setParticipant($participant);
        $spending->setDepense($spend);


        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();

        return $this->redirectToRoute('campaign_show', [
            'id' => $request->request->get('campaign_id')
        ]);

    }
}