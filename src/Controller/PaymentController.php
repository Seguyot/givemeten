<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Participant;
use App\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
  /**
     * @Route("/charge", name="payment_charge", methods="GET|POST")
     */
    public function charge(Request $request): Response
    {

        $campaign_id =  $request->request->get('campaign_id');
        $amount =(int)$request->request->get("amount") *100;
        
        try{
        \Stripe\Stripe::setApiKey('sk_live_cmR0ZtFFSV5jEWWe9NMQ7bnu');
        $charge = \Stripe\Charge::create(['amount'=>$amount, 'currency'=>'eur','source'=>
        $request->request->get("stripeToken")]);
        }
         catch (\Exception $e) {
            $this->addFlash( 
            'error',
            'Le paiement à échoué. Raison'. $e->getMessage()
            );
            return $this->redirectToRoute('campaign_pay',[
                "id"=>$campaign_id
            ]);
            //ToDo:Gérer les messages spéciaux
         }
    
        $campaign=$this->getDoctrine()
        ->getRepository(Campaign::class)
        ->find($campaign_id);
        
        $participant = new Participant();
        $participant->setCampaign($campaign);
        $participant->setName($request->request->get("name"));
        $participant->setEmail($request->request->get("email"));

        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();
       
        $payment = new Payment();
        $payment->setAmount ($amount);
        $payment->setParticipant($participant);

       

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();


        
         return $this ->redirectToroute('campaign_show', [
             "id" => $request->request->get("campaign_id")
         ]);
    }
}